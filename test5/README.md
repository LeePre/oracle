# 实验5：包，过程，函数的用法

- 学号：202010414218，姓名：王旭鹏，班级：2

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。 Oracle递归查询的语句格式是：

- 实验步骤

- 步骤1：登录用户hr，创建名为MyPack的包
  
  ```sql
  create or replace PACKAGE MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
  END MyPack;
  ```
  
  ![](1.png)
  ![](1-2.png)
- 步骤2：在MyPack中创建Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。在MyPack中创建过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。 

  ```sql
  create or replace PACKAGE BODY MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
  AS
      N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
      BEGIN
      SELECT SUM(salary) into N  FROM EMPLOYEES E
      WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
      RETURN N;
      END;
  
  PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
  AS
      LEFTSPACE VARCHAR(2000);
      begin
      --通过LEVEL判断递归的级别
      LEFTSPACE:=' ';
      --使用游标
      for v in
          (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
          START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
          CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
      LOOP
          DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                              V.EMPLOYEE_ID||' '||v.FIRST_NAME);
      END LOOP;
      END;
  END MyPack;
  ```
  ![](2.png)

3. 步骤3：Get_SalaryAmount()测试
    
    ```sql
    select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
    ```
   
   ![](3.png)
   
3. 步骤4：Get_Employees()测试
    
    ```sql
    set serveroutput on
    DECLARE
   V_EMPLOYEE_ID NUMBER;    
   BEGIN
   V_EMPLOYEE_ID := 101;
   MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
   END;
   ```
   ![](4.png)
   
   

## 结论

本次实验主要涉及到Oracle中的包(Package)以及函数和过程的创建和使用，以及递归查询的实现方式。并且在包中创建了一个函数Get_SalaryAmount及过程过程GET_EMPLOYEES。在Get_SalaryAmount函数中通过部门ID查询员工表便会统计每个部门的工资，在GET_EMPLOYEES过程中通过员工ID参数查询员工表，就可以查询某个员工及其所有下属，子下属员工。总的来说，本次实验了解了Oracle中包(Package)的使用方法，以及如何创建函数和过程
