# 软工二班 202010414218 王旭鹏

# 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

# 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

# 实验步骤
## 步骤1
以system登录到pdborcl，创建角色con和用户saee，并授权和分配空间：
$ sqlplus system/123@pdborcl
CREATE ROLE con;
GRANT connect,resource,CREATE VIEW TO con;
CREATE USER saee IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER saee default TABLESPACE "USERS";
ALTER USER saee QUOTA 50M ON users;
GRANT con TO saee;
--收回角色
REVOKE con FROM saee;
![1](1.png)
## 步骤2
	新用户saee连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。
	$ sqlplus saee/123@pdborcl
	show user;
	SELECT * FROM session_privs;
	SELECT * FROM session_roles;
	CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
	INSERT INTO customers(id,name)VALUES(1,'zhang');
	INSERT INTO customers(id,name)VALUES (2,'wang');
	CREATE VIEW customers_view AS SELECT name FROM customers;
	GRANT SELECT ON customers_view TO hr;
	SELECT * FROM customers_view;
	
![](2-1.png)
![](2-2.png)

## 步骤3
	用户hr连接到pdborcl，查询saee授予它的视图customers_view
	$ sqlplus hr/123@pdborcl
	SELECT * FROM saee.customers;
	select * from saee.customers
	SELECT * FROM saee.customers_view;
![](3-1.png)

## 步骤4
	概要文件设置,用户最多登录时最多只能错误3次
	$ sqlplus system/123@pdborcl
	ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
![](4-1.png)

## 步骤5
	查看数据库的使用情况
	$ sqlplus system/123@pdborcl
	
	SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';
	
	SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
	free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
	Round(( total - free )/ total,4)* 100 "使用率%"
	from (SELECT tablespace_name,Sum(bytes)free
	    FROM   dba_free_space group  BY tablespace_name)a,
	   (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
	    group  BY tablespace_name)b
	where  a.tablespace_name = b.tablespace_name;
![](5-1.png)

## 步骤6
	实验结束删除用户和角色
	$ sqlplus system/123@pdborcl
	drop role con;
	drop user saee cascade;
![](./6-1.png)

# 结论
在本次实验中，我们学习了如何在Oracle数据库中创建角色和用户，并进行权限管理。通过创建一个新的本地角色con_res_role，并授予其connect、resource和CREATE VIEW权限，我们使得任何拥有该角色的用户都可以创建表、过程、触发器和视图。接着，我们创建了一个名为sale的用户，并将con_res_role角色授予给该用户，从而赋予其相关权限。最后，通过测试验证了sale用户成功获得了相应的权限，可以进行相关操作。这次实验使我们更加熟悉了Oracle数据库的角色和权限管理，为以后的数据库管理工作打下了基础。