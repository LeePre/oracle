﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 学号：202010414218，姓名：王旭鹏，班级：2

## 实验目的
设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 实验要求
- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项     | 评分标准                             | 满分 |
| :--------- | :----------------------------------- | :--- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |

## 实验步骤

#### 1、设计表空间

主表空间：主表空间用于存储主要的表和数据；索引表空间：用于存储索引数据，这样可以提高查询性能。通过合理划分表空间，可以更好地管理数据、控制存储和优化性能。使用现有的数据库连接在orcl_system中设计表空间一个名为main_ts.dbf；另一个名为index_ts.dbf；

```sql
CREATE TABLESPACE main_ts DATAFILE 'main_ts.dbf' SIZE 100M;
CREATE TABLESPACE index_ts DATAFILE 'index_ts.dbf' SIZE 50M;
```

![](第一个表空间.png)

![](第二个表空间.png)

#### 2、设计表

商品信息表：
```sql
CREATE TABLE product (
product_id NUMBER,
product_name VARCHAR2(100),
price NUMBER,
stock_quantity NUMBER,
supplier_id NUMBER,
CONSTRAINT product_pk PRIMARY KEY (product_id)
) TABLESPACE main_ts;

```
![](商品信息表.png)
订单信息表：
```sql
CREATE TABLE product (
product_id NUMBER,
product_name VARCHAR2(100),
price NUMBER,
stock_quantity NUMBER,
supplier_id NUMBER,
CONSTRAINT product_pk PRIMARY KEY (product_id)
) TABLESPACE main_ts;

```
![](订单信息表.png)
用户表：
```sql
CREATE TABLE customer (
customer_id NUMBER,
customer_name VARCHAR2(100),
contact VARCHAR2(100),
CONSTRAINT customer_pk PRIMARY KEY (customer_id)
) TABLESPACE main_ts;


```
![](客户信息表.png)
供应商表：
```sql
CREATE TABLE supplier (
supplier_id NUMBER,
supplier_name VARCHAR2(100),
contact VARCHAR2(100),
CONSTRAINT supplier_pk PRIMARY KEY (supplier_id)
) TABLESPACE main_ts;


```
![](供应商信息表.png)
#### 3、插入数据
```sql
-- 向商品信息表插入数据（5万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO product (product_id, product_name, price, stock_quantity, supplier_id)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 1000)), ROUND(DBMS_RANDOM.VALUE(1, 100)));
  END LOOP;
  COMMIT;
END;
/

```
![](商品信息插入.png)
```sql
-- 向订单信息表插入数据（1万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..10000 LOOP
    INSERT INTO orders (order_id, order_date, order_amount, customer_id)
    VALUES (i, SYSDATE, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 1000)));
  END LOOP;
  COMMIT;
END;
/

```
![](订单信息插入.png)
```sql
-- 向客户信息表插入数据（5万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO customer (customer_id, customer_name, contact)
    VALUES (i, 'Customer ' || i, 'Contact ' || i);
  END LOOP;
  COMMIT;
END;
/

```
![](客户信息插入.png)
```sql
-- 向供应商信息表插入数据（3万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO supplier (supplier_id, supplier_name, contact)
    VALUES (i, 'Supplier ' || i, 'Contact ' || i);
  END LOOP;
  COMMIT;
END;
/

```
![](供应商信息插入.png)


#### 4、创建用户及分配权限
创建管理员用户admin1 密码admin123，并分配权限
```sql
-- 创建管理员用户
CREATE USER C##admin1 IDENTIFIED BY admin123;
GRANT CONNECT, RESOURCE, DBA TO C##admin1;

```
![](管理员用户.png)
创建普通用户C##user1 密码user123，并分配权限
-- 创建普通用户
```sql
-- 创建普通用户
CREATE USER C##user1 IDENTIFIED BY user123;
GRANT CONNECT, RESOURCE TO C##user1;

```
![](普通用户.png)
#### 5、建立程序包
创建程序包程序包中定义了四个过程和函数：calculate_order_total（计算订单总金额）、update_product_stock（更新商品库存量）、get_customer_orders（查找特定客户订单信息）和get_supplier_info（根据供应商ID查询供应商信息）。
```sql
-- 创建普通用户
create or replace NONEDITIONABLE PACKAGE sales_pkg AS
-- 计算订单总金额
FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER;

-- 更新商品库存量
PROCEDURE update_product_stock(p_product_id IN NUMBER,
    p_quantity IN NUMBER);

-- 查询特定客户的订单信息
FUNCTION get_customer_orders(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR;

-- 根据供应商ID查询供应商信息
FUNCTION get_supplier_info(supplier_id IN NUMBER) RETURN supplier%ROWTYPE;
END sales_pkg;

```
![](创建程序包.png)
#### 6、设计存储函数
```sql
create or replace NONEDITIONABLE PACKAGE BODY sales_pkg AS
  -- 计算订单总金额
  FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER IS
    total_amount NUMBER;
  BEGIN
    SELECT SUM(order_amount)
    INTO total_amount
    FROM orders
    WHERE order_id = order_id;

    RETURN total_amount;
  END calculate_order_total;
-- 更新商品库存量
PROCEDURE update_product_stock(
    p_product_id IN NUMBER,
    p_quantity IN NUMBER
) IS
    v_current_stock NUMBER;
BEGIN
    SELECT stock_quantity
    INTO v_current_stock
    FROM product
    WHERE product_id = p_product_id
    FOR UPDATE;

    IF v_current_stock >= p_quantity THEN
        UPDATE product
        SET stock_quantity = stock_quantity - p_quantity
        WHERE product_id = p_product_id;
        COMMIT; -- 提交事务
        DBMS_OUTPUT.PUT_LINE('Product stock updated successfully.');
    ELSE
        RAISE_APPLICATION_ERROR(-20001, 'Insufficient stock.');
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20002, 'Product not found.');
END update_product_stock;


-- 查询特定客户的订单信息
FUNCTION get_customer_orders(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR IS
      orders_cur SYS_REFCURSOR;
    BEGIN
      OPEN orders_cur FOR
        SELECT *
        FROM orders
        WHERE customer_id = p_customer_id;

      RETURN orders_cur;
END get_customer_orders;


  -- 根据供应商ID查询供应商信息
FUNCTION get_supplier_info(supplier_id IN NUMBER) RETURN supplier%ROWTYPE IS
      supplier_rec supplier%ROWTYPE;
    BEGIN
      SELECT *
      INTO supplier_rec
      FROM supplier
      WHERE supplier_id = supplier_id
      FETCH FIRST 1 ROW ONLY; -- 限制返回的行数为1行
    
      RETURN supplier_rec;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN -- 处理未找到数据的情况
        RETURN NULL; -- 返回空值或自定义的空记录
 END get_supplier_info;
 END sales_pkg;

```
![](实现四个函数.png)
#### 7、存储函数调用
1.	调用计算订单总金额的函数并打印结果：
```sql
SET SERVEROUTPUT ON
DECLARE
  order_total NUMBER;
BEGIN
  order_total := sales_pkg.calculate_order_total(1); 
  DBMS_OUTPUT.PUT_LINE('Order Total: ' || order_total);
END;

```
![](调用金额统计.png)
2.	调用更新商品库存量的存储过程并打印消息：
```sql
BEGIN
  sales_pkg.update_product_stock(456, 10);  -- 传入商品ID和数量
  DBMS_OUTPUT.PUT_LINE('Product stock updated successfully.');
END;
/

```
![](调用更新库存.png)
3.	调用查询特定客户的订单信息并打印消息：
```sql
DECLARE
  -- 定义变量
  customer_id_param NUMBER := 123; -- 用于传递给函数的客户ID
  orders_result SYS_REFCURSOR; -- 存储函数返回的结果集

  -- 定义变量来存储结果集中的列值
  order_id orders.order_id%TYPE;
  order_date orders.order_date%TYPE;
  order_amount orders.order_amount%TYPE;
  customer_id orders.customer_id%TYPE;

BEGIN
  -- 调用函数并获取结果集
  orders_result := sales_pkg .get_customer_orders(customer_id_param);

  -- 使用循环遍历结果集中的每一行
  LOOP
    -- 从结果集中提取值到变量中
    FETCH orders_result INTO order_id, order_date, order_amount, customer_id;
    EXIT WHEN orders_result%NOTFOUND; -- 当没有更多行时退出循环

    -- 在这里可以对每行数据进行处理，例如打印或进行其他操作
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || order_id);
    DBMS_OUTPUT.PUT_LINE('Order Date: ' || order_date);
    DBMS_OUTPUT.PUT_LINE('Order Amount: ' || order_amount);
    DBMS_OUTPUT.PUT_LINE('Customer ID: ' || customer_id);
  END LOOP;

  -- 关闭结果集
  CLOSE orders_result;

EXCEPTION
  -- 处理异常
  WHEN OTHERS THEN
    -- 在这里处理异常情况，例如打印错误信息或进行其他操作
    DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
END;

```
![](调用特定客户订单统计.png)
4.	根据供应商ID查询供应商信息：
```sql
SET SERVEROUTPUT ON
DECLARE
  -- 定义变量
  supplier_id_param NUMBER := 12; -- 用于传递给函数的供应商ID
  supplier_info supplier%ROWTYPE; -- 存储函数返回的供应商信息

BEGIN
  -- 调用函数并获取供应商信息
  supplier_info := sales_pkg.get_supplier_info(supplier_id_param);

  -- 打印供应商信息
  DBMS_OUTPUT.PUT_LINE('Supplier ID: ' || supplier_info.supplier_id);
  DBMS_OUTPUT.PUT_LINE('Supplier Name: ' || supplier_info.supplier_name);
  DBMS_OUTPUT.PUT_LINE('Contact: ' || supplier_info.contact);
  -- 可以继续打印其他供应商信息字段

EXCEPTION
  -- 处理异常
  WHEN OTHERS THEN
    -- 在这里处理异常情况，例如打印错误信息或进行其他操作
    DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
END;

```
![](调用供应商查询.png)
#### 8、备份
1.	备份策略：
•	每天凌晨2点进行完全备份。
•	每周六凌晨2点进行增量备份。
•	每月第一天凌晨2点进行归档备份。
2.	备份类型：
•	使用物理备份工具（如Oracle RMAN）执行数据库备份。
•	执行完全备份时，备份数据库的数据文件、控制文件和日志文件。
•	执行增量备份时，仅备份自上次完全备份以来的增量变更。
3.	备份存储位置：
•	将备份数据存储在独立的磁盘阵列或网络存储设备上。
•	确保备份数据与数据库主机分离，以防止单点故障。
•	定期将备份数据复制到离线介质（如磁带库）或远程存储服务器作为灾难恢复手段。
4.	自动化备份过程：
•	创建定期任务或计划，每天凌晨2点自动触发完全备份。
•	每周六凌晨2点自动触发增量备份。
•	每月第一天凌晨2点自动触发归档备份。
5.	验证备份完整性：
•	每周对完全备份进行验证和恢复测试，确保备份的完整性和可恢复性。
•	使用RMAN或其他恢复工具执行恢复测试，验证备份数据的有效性。
6.	灾难恢复计划：
•	编写详细的灾难恢复计划，包括备份恢复步骤、应急联系人信息和备份数据的存储位置。
•	定期审查和更新灾难恢复计划，以确保与业务需求和系统变更的一致性。

在命令行中运行RMAN指令连接数据库并备份
![](RMAN备份连接.png)
完全备份
```sql
-- 完全备份脚本
RMAN> RUN {
     ALLOCATE CHANNEL ch1 DEVICE TYPE disk;
     BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
     RELEASE CHANNEL ch1;
}

```
![](完全备份1.png)
![](完全备份2.png)
```sql
-- 增量备份脚本
RMAN> RUN {
     ALLOCATE CHANNEL ch1 DEVICE TYPE disk;
     BACKUP INCREMENTAL LEVEL 1 FOR RECOVER OF COPY WITH TAG 'INCREMENTAL_BACKUP' DATABASE PLUS ARCHIVELOG;
     RELEASE CHANNEL ch1;
}

```
![](增量备份1.png)
![](增量备份2.png)
![](增量备份3.png)
```sql
-- 归档备份脚本
RMAN> RUN {
     ALLOCATE CHANNEL ch1 DEVICE TYPE disk;
     BACKUP ARCHIVELOG ALL NOT BACKED UP 1 TIMES;
     RELEASE CHANNEL ch1;
}

```
![](归档备份.png)
## 结论

- 本次实验项目的设计和实现，成功构建了一个基于Oracle数据库的商品销售系统，满足了商场对商品销售流程管理的需求。但还是有很多不足，功能层面来说有点少，由于时间问题没有做的很完善。系统具备良好的数据结构和业务逻辑，能够高效地处理订单、库存和供应商等相关操作，提高了工作效率和数据管理的准确性。在将来的学习中还需努力。
