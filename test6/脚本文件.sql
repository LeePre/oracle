CREATE TABLESPACE main_ts DATAFILE 'main_ts.dbf' SIZE 100M;
CREATE TABLESPACE index_ts DATAFILE 'index_ts.dbf' SIZE 50M;

CREATE TABLE product (
product_id NUMBER,
product_name VARCHAR2(100),
price NUMBER,
stock_quantity NUMBER,
supplier_id NUMBER,
CONSTRAINT product_pk PRIMARY KEY (product_id)
) TABLESPACE main_ts;

CREATE TABLE product (
product_id NUMBER,
product_name VARCHAR2(100),
price NUMBER,
stock_quantity NUMBER,
supplier_id NUMBER,
CONSTRAINT product_pk PRIMARY KEY (product_id)
) TABLESPACE main_ts;

CREATE TABLE customer (
customer_id NUMBER,
customer_name VARCHAR2(100),
contact VARCHAR2(100),
CONSTRAINT customer_pk PRIMARY KEY (customer_id)
) TABLESPACE main_ts;

CREATE TABLE supplier (
supplier_id NUMBER,
supplier_name VARCHAR2(100),
contact VARCHAR2(100),
CONSTRAINT supplier_pk PRIMARY KEY (supplier_id)
) TABLESPACE main_ts;

-- 向商品信息表插入数据（5万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO product (product_id, product_name, price, stock_quantity, supplier_id)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 1000)), ROUND(DBMS_RANDOM.VALUE(1, 100)));
  END LOOP;
  COMMIT;
END;
/
-- 向订单信息表插入数据（1万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..10000 LOOP
    INSERT INTO orders (order_id, order_date, order_amount, customer_id)
    VALUES (i, SYSDATE, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 1000)));
  END LOOP;
  COMMIT;
END;
/

-- 向客户信息表插入数据（5万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO customer (customer_id, customer_name, contact)
    VALUES (i, 'Customer ' || i, 'Contact ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 向供应商信息表插入数据（3万条）
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..30000 LOOP
    INSERT INTO supplier (supplier_id, supplier_name, contact)
    VALUES (i, 'Supplier ' || i, 'Contact ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 创建管理员用户
CREATE USER C##admin1 IDENTIFIED BY admin123;
GRANT CONNECT, RESOURCE, DBA TO C##admin1;

-- 创建普通用户
CREATE USER C##user1 IDENTIFIED BY user123;
GRANT CONNECT, RESOURCE TO C##user1;

-- 创建普通用户
create or replace NONEDITIONABLE PACKAGE sales_pkg AS
-- 计算订单总金额
FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER;

-- 更新商品库存量
PROCEDURE update_product_stock(p_product_id IN NUMBER,
    p_quantity IN NUMBER);

-- 查询特定客户的订单信息
FUNCTION get_customer_orders(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR;

-- 根据供应商ID查询供应商信息
FUNCTION get_supplier_info(supplier_id IN NUMBER) RETURN supplier%ROWTYPE;
END sales_pkg;

create or replace NONEDITIONABLE PACKAGE BODY sales_pkg AS
  -- 计算订单总金额
  FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER IS
    total_amount NUMBER;
  BEGIN
    SELECT SUM(order_amount)
    INTO total_amount
    FROM orders
    WHERE order_id = order_id;

    RETURN total_amount;
  END calculate_order_total;
-- 更新商品库存量
PROCEDURE update_product_stock(
    p_product_id IN NUMBER,
    p_quantity IN NUMBER
) IS
    v_current_stock NUMBER;
BEGIN
    SELECT stock_quantity
    INTO v_current_stock
    FROM product
    WHERE product_id = p_product_id
    FOR UPDATE;

    IF v_current_stock >= p_quantity THEN
        UPDATE product
        SET stock_quantity = stock_quantity - p_quantity
        WHERE product_id = p_product_id;
        COMMIT; -- 提交事务
        DBMS_OUTPUT.PUT_LINE('Product stock updated successfully.');
    ELSE
        RAISE_APPLICATION_ERROR(-20001, 'Insufficient stock.');
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20002, 'Product not found.');
END update_product_stock;


-- 查询特定客户的订单信息
FUNCTION get_customer_orders(p_customer_id IN NUMBER) RETURN SYS_REFCURSOR IS
      orders_cur SYS_REFCURSOR;
    BEGIN
      OPEN orders_cur FOR
        SELECT *
        FROM orders
        WHERE customer_id = p_customer_id;

      RETURN orders_cur;
END get_customer_orders;


  -- 根据供应商ID查询供应商信息
FUNCTION get_supplier_info(supplier_id IN NUMBER) RETURN supplier%ROWTYPE IS
      supplier_rec supplier%ROWTYPE;
    BEGIN
      SELECT *
      INTO supplier_rec
      FROM supplier
      WHERE supplier_id = supplier_id
      FETCH FIRST 1 ROW ONLY; -- 限制返回的行数为1行
    
      RETURN supplier_rec;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN -- 处理未找到数据的情况
        RETURN NULL; -- 返回空值或自定义的空记录
 END get_supplier_info;
 END sales_pkg;

SET SERVEROUTPUT ON
DECLARE
  order_total NUMBER;
BEGIN
  order_total := sales_pkg.calculate_order_total(1); 
  DBMS_OUTPUT.PUT_LINE('Order Total: ' || order_total);
END;

BEGIN
  sales_pkg.update_product_stock(456, 10);  -- 传入商品ID和数量
  DBMS_OUTPUT.PUT_LINE('Product stock updated successfully.');
END;
/


DECLARE
  -- 定义变量
  customer_id_param NUMBER := 123; -- 用于传递给函数的客户ID
  orders_result SYS_REFCURSOR; -- 存储函数返回的结果集

  -- 定义变量来存储结果集中的列值
  order_id orders.order_id%TYPE;
  order_date orders.order_date%TYPE;
  order_amount orders.order_amount%TYPE;
  customer_id orders.customer_id%TYPE;

BEGIN
  -- 调用函数并获取结果集
  orders_result := sales_pkg .get_customer_orders(customer_id_param);

  -- 使用循环遍历结果集中的每一行
  LOOP
    -- 从结果集中提取值到变量中
    FETCH orders_result INTO order_id, order_date, order_amount, customer_id;
    EXIT WHEN orders_result%NOTFOUND; -- 当没有更多行时退出循环

    -- 在这里可以对每行数据进行处理，例如打印或进行其他操作
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || order_id);
    DBMS_OUTPUT.PUT_LINE('Order Date: ' || order_date);
    DBMS_OUTPUT.PUT_LINE('Order Amount: ' || order_amount);
    DBMS_OUTPUT.PUT_LINE('Customer ID: ' || customer_id);
  END LOOP;

  -- 关闭结果集
  CLOSE orders_result;

EXCEPTION
  -- 处理异常
  WHEN OTHERS THEN
    -- 在这里处理异常情况，例如打印错误信息或进行其他操作
    DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
END;

SET SERVEROUTPUT ON
DECLARE
  -- 定义变量
  supplier_id_param NUMBER := 12; -- 用于传递给函数的供应商ID
  supplier_info supplier%ROWTYPE; -- 存储函数返回的供应商信息

BEGIN
  -- 调用函数并获取供应商信息
  supplier_info := sales_pkg.get_supplier_info(supplier_id_param);

  -- 打印供应商信息
  DBMS_OUTPUT.PUT_LINE('Supplier ID: ' || supplier_info.supplier_id);
  DBMS_OUTPUT.PUT_LINE('Supplier Name: ' || supplier_info.supplier_name);
  DBMS_OUTPUT.PUT_LINE('Contact: ' || supplier_info.contact);
  -- 可以继续打印其他供应商信息字段

EXCEPTION
  -- 处理异常
  WHEN OTHERS THEN
    -- 在这里处理异常情况，例如打印错误信息或进行其他操作
    DBMS_OUTPUT.PUT_LINE('An error occurred: ' || SQLERRM);
END;


